# JavaScript 数组常用 API
##1、sort( [compareFunction] ) 
对数组做原地排序，并返回这个数组，默认按照字符串 UNICODE 的码位点排序，如下所示：

```js
var fruit = ['cherries', 'apples', 'bananas'];
fruit.sort(); // ['apples', 'bananas', 'cherries']
 
var scores = [1, 10, 2, 21];
scores.sort(); // [1, 10, 2, 21]

```
这显然不是我们想要的结果。为了实现真正意义上的排序，可以给 sort 传递一个回调函数 compareFunction， 其接收两个参数 a 和 b，分别代表数组中待比较的两个元素，a 元素 排在 b 的前面 。

回调函数需要返回一个表达式，用以标明 升序 或 降序 操作：
* return a - b ：如果表达式 a - b 为真，触发交换操作。也就是说， 当 a > b 时，进行元素交换，让较小的元素 b 排在较大的元素 a 前面，即 升序操作。
* return b - a ：如果表达式 b - a 为真，触发交换操作。也就是说， 当 a < b 时，进行元素交换，让较大的元素 b 排在较小的元素 a 前面，即 降序操作。

```js
var numbers = [2,4,1,10,3];
 
// 回调参数 a,b 是数组要比较的两个元素，a 排在 b 的前面。
numbers.sort(function(a,b){
    // 当 a > b 时触发交换操作，把较小的排在前面，即升序。
    return a - b;
}); //[1,2,3,4,10]
 
numbers.sort(function(a,b){
    // 当 a < b 时触发交换操作，把较大的排在前面，即降序。
    return b - a;
}); //[10,4,3,2,1]
```
##2、join( [separator] )
将数组中的所有元素连接成一个字符串。separtor 用于指定连接每个数组元素的分隔符。分隔符会被转成字符串类型；如果省略的话，默认为一个逗号。如果separtor 是一个空字符串，那么数组中的所有元素将被直接连接。

```js
var data = ['Wind', 'Rain', 'Fire'];
data.join();      //Wind,Rain,Fire
data.join(', ');  //Wind, Rain, Fire
data.join(' + '); //Wind + Rain + Fire
data.join('');    //WindRainFire
```
##3、concat( value1,...,valueN )
concat 方法将创建一个新的数组，然后将调用它的对象(this 指向的对象)中的元素以及所有参数中的数组类型的参数中的元素以及非数组类型的参数本身按照顺序放入这个新数组，并返回该数组， valueN 允许是数组或非数组值。在没有给 concat 传递参数的情况下，它只是复制当前数组并返回副本。 

```js
var alpha = ['a', 'b', 'c'];
 
alpha.concat(1, [2, 3]); //["a", "b", "c", 1, 2, 3]
 
alpha.concat(); // ['a', 'b', 'c']
```
##4、push( element1,...,elementN )  和  pop( )
push 添加一个或多个元素到数组的末尾，并返回数组新的长度；pop删除一个数组中的最后的一个元素，并且返回这个元素。

```js
var data = [1,2,3];
 
data.push(4,5,6); //6 ->数组的长度  data->[1,2,3,4,5,6]
 
data.pop(); //6 ->出栈的元素 data->[1,2,3,4,5]
```
注意：push 和 pop 并不会改变原来的元素位置。
##5、unshift( element1,...,elementN )  和  shift( )
unshift 添加一个或多个元素到数组的开头，并返回数组新的长度；shift 删除一个数组中的第一个元素，并且返回这个元素。

```js
var data = [1,2,3];
 
data.unshift(-1,-2,-3); //6->新数组的长度 data->[-1,-2,-3,1,2,3]
 
data.shift(); // -1 ->被移除的元素 data->[-2,-3,1,2,3]
```

注意：unshift 和 shift 都会改变原来的元素位置。

如果把数组看成一个栈，push 和 pop 、unshift 和 shift 对数组的操作行为就符合 后进先出 (LIFO) 规律 ；如果把数组看成一个队列，push 和 shift 、unshift 和 pop 对数组的操作行为就符合 先进先出 (FIFO) 规律。

因此，可以使用数组来实现对应的数据结构：栈 和 队列。
##6、slice( begin[,end] ) 
slice 方法从begin 的索引位置开始提取数组元素，到 end 位置结束，但不包括 end 位置的元素，如果 end 被省略，则默认提取到数组的结尾，如果结束位置小于起始位置，则返回空数组。 

```js
var data = [1,2,3];
 
data.slice(0); //[1,2,3] 提取的元素
data.slice(1,2); //[2] 提取的元素
data.slice(2,1); //[]
```

如果参数中有一个负数，则用数组长度加上该数来确定相应的位置。例如，在一个包含 5 项的数组上调用 slice(-2,-1) 与调用  slice(3,4) 得到的结果相同。

```js
var t = [1,2,3,4,5];
t.slice(-2,-1); // [4]
t.slice(3,4);   // [4]
```
slice方法会返回一个新的数组，由于数组是引用类型， 通常会通过 arr.slice(0) 来实现数组的 浅拷贝，但这只对 数组元素是基本类型 的情况有效。

```js
//简单数组拷贝
var arr = [1,2,3,4];
var cyarr = arr.slice(0);  //浅拷贝
arr.splice(3,1); //对原数组操作
 
console.log(arr,cyarr); //[1,2,3] , [1,2,3,4]  -> 拷贝成功
```
如果是对象数组，数组元素作为指针指向对象内存，slice(0) 仅仅是拷贝了一份指针作为副本，而副本中的指针，指向的还是原来的对象，因此，对一个数组中的对象做操作，另一个数组中的对象也会同步变化。

```js
//对象数组拷贝
var list = [{name:'zhangsan'}];
var cylist = list.slice(0); //浅拷贝
list[0].name = 'lisi'; //对原数组操作
 
console.log(list,cylist); // [{name:'lisi'}] , [{name:'lisi'}] -> 拷贝失败　　
```
要实现数组的深拷贝，需要通过 JSON 的序列化和反序列化来实现，即： JSON.parse( JSON.stringify(arr) )

```js
//对象数组拷贝
var list = [{name:'zhangsan'}];
var cylist = JSON.parse(JSON.stringify(list)); //深拷贝
list[0].name = 'lisi'; //对原数组操作
 
console.log(list,cylist); // [{name:'lisi'}] , [{name:'zhangsan'}] -> 拷贝成功　　
```
##7、splice( start, deleteCount[,value1,...,valueN] )
splice方法从一个数组中移除一个或多个元素，如果必要，在所移除元素的位置上插入新元素，返回所移除的元素。

```js
data.splice(2,1);//[3]->被删除的元素 data->[1,2]
data.splice(2,2,4,5); //[3]->被删除的元素 data->[1,2,4,5]
data.splice(2,2,4,5,6); //[3]->被删除的元素 data->[1,2,4,5,6]
```


