## Web 前端工程师课程 第3周 (new)
### [CSDN链接地址](http://edu.csdn.net/course/detail/6424)

> 第三周涉及数据操作，对象操作，异步函数等
> 正则表达式
> 数组的理解及其操作
> 字符串的理解及其操作
> 一些常用的内置对象：Date，Math...
> Promise的用法
> async 函数的使用

